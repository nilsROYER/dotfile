-- plugins
require("plugins")

-- config
require("config.settings")
require("config.keymaps")

-- require("themes.onedark")
-- require("themes.kanaguya")
require("themes.tokyonight")
require("plugins.lualine")
