local dashboard_ok, dashboard = pcall(require, "alpha")
local startify_ok, startify = pcall(require, "alpha.themes.startify")
if not dashboard_ok then
    return
end
if not startify_ok then
    return
end

dashboard.setup(startify.config)
