local null_ls_ok, null_ls = pcall(require, "null-ls")
if not null_ls_ok then
	return
end

local defaults = {
	border = nil,
	cmd = { "nvim" },
	debounce = 250,
	debug = false,
	default_timeout = 5000,
	diagnostic_config = {},
	diagnostics_format = "#{m}",
	fallback_severity = vim.diagnostic.severity.ERROR,
	log_level = "warn",
	notify_format = "[null-ls] %s",
	on_attach = true,
	on_init = nil,
	on_exit = nil,
	root_dir = require("null-ls.utils").root_pattern(".null-ls-root", "Makefile", ".git"),
	should_attach = true,
	sources = nil,
	temp_dir = nil,
	update_in_insert = false,
}

null_ls.setup({
	defaults,
	sources = {
		null_ls.builtins.formatting.stylua,
		null_ls.builtins.diagnostics.eslint_d,
		null_ls.builtins.diagnostics.php,
		null_ls.builtins.diagnostics.phpcs,
		null_ls.builtins.diagnostics.phpstan,
		null_ls.builtins.diagnostics.sqlfluff.with({
			extra_args = { "--dialect", "mysql" }, -- change to your dialect
		}),
		null_ls.builtins.code_actions.gitsigns,
	},
})
