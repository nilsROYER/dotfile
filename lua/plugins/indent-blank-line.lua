local indent_ok, indent = pcall(require, "indent_blankline")
if not indent_ok then
    return
end

local opt = vim.opt
opt.list = true
opt.listchars:append("space:.")
opt.listchars:append("eol:↴")

indent.setup({
    show_end_of_line = true,
    show_current_context = true,
    show_current_context_start = true,
})
