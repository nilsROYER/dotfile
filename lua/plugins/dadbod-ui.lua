vim.g.db_ui_disable_mappings = 0
vim.g.db_ui_save_location = '~/.config/nvim/lua/dadbod-ui/db'
vim.g.db_ui_win_position = 'right'
vim.g.db_ui_winwidth = 60
vim.g.db_ui_show_database_icon = 1
vim.g.db_ui_use_nerd_fonts = 1
