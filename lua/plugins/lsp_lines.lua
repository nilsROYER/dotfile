local lsp_lines_ok, lsp_lines = pcall(require, "lsp_lines")
if not lsp_lines_ok then
	return
end

lsp_lines.setup()

-- Disable virtual_text since it's redundant due to lsp_lines.
vim.diagnostic.config({
	virtual_text = false,
})
