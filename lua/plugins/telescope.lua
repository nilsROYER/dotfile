local telescope_ok, telescope = pcall(require, 'telescope')
if not telescope_ok then
    return
end

local buildin_ok, builtin = pcall(require, 'telescope.builtin')
if not buildin_ok then
    return
end

local actions_ok, actions = pcall(require, 'telescope.actions')
if not actions_ok then
    return
end

vim.keymap.set('n', '<C-n>', builtin.find_files, {})
vim.keymap.set('n', '<C-f>', builtin.live_grep, {})
vim.keymap.set('n', '<leader>fb', builtin.buffers, {})
vim.keymap.set('n', '<leader>fh', builtin.help_tags, {})

telescope.setup {
    defaults = {
        mappings = {
            i = {
                ["<CR>"] = actions.select_tab
            }
        }
    },
}

telescope.load_extension("notify")
