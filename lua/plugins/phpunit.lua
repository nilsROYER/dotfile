local neotest_ok, neotest = pcall(require, 'neotest')
local phpunit_ok, phpunit = pcall(require, 'neotest-phpunit')
if not neotest_ok then
    return
end

if not phpunit_ok then
    return
end

neotest.setup({
    library = { plugins = { "neotest" }, types = true },
    icons = {
        expanded = "",
        child_prefix = "",
        child_indent = "",
        final_child_prefix = "",
        non_collapsible = "",
        collapsed = "",

        passed = "",
        running = "",
        failed = "",
        unknown = ""
    },
    adapters = {
        require('neotest-phpunit')({
            phpunit_cmd = {
                "docker",
                "compose",
                "exec",
                "php",
                "php",
                "vendor/bin/phpunit",
            },
            result_path = './phpunit.result.cache',
            root_files = { "composer.json", "phpunit.xml", ".gitignore" },
            filter_dirs = { ".git", "node_modules" },
        })
    },
})
