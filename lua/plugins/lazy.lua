-- Bootstrap lazy.nvim
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not (vim.uv or vim.loop).fs_stat(lazypath) then
    local lazyrepo = "https://github.com/folke/lazy.nvim.git"
    local out = vim.fn.system({ "git", "clone", "--filter=blob:none", "--branch=stable", lazyrepo, lazypath })
    if vim.v.shell_error ~= 0 then
        vim.api.nvim_echo({
            { "Failed to clone lazy.nvim:\n", "ErrorMsg" },
            { out,                            "WarningMsg" },
            { "\nPress any key to exit..." },
        }, true, {})
        vim.fn.getchar()
        os.exit(1)
    end
end
vim.opt.rtp:prepend(lazypath)

-- load lazy
local lazy_ok, lazy = pcall(require, "lazy");
if not lazy_ok then
    print('lazy not loaded')
    return;
end

-- Cnfig lazy
lazy.setup({
    spec = {
        {
            "vhyrro/luarocks.nvim",
            priority = 1000, -- Very high priority is required, luarocks.nvim should run as the first plugin in your config.
            config = true,
        },
        -- color theme
        { "olimorris/onedarkpro.nvim", lazy = true },
        { "rebelot/kanagawa.nvim",     lazy = true },
        { "folke/tokyonight.nvim",     lazy = true },
        { "xiyaowong/transparent.nvim", lazy = false },
        -- more lua function
        { "nvim-lua/plenary.nvim",     lazy = true },
        -- lua api Autocompletion
        { "folke/neodev.nvim",         lazy = true },
        -- file finder
        {
            "nvim-telescope/telescope.nvim",
            tag = "0.1.2",
            -- or                            , branch = '0.1.x',
            dependencies = { { "nvim-lua/plenary.nvim" } },
            lazy = true
        },
        -- LSP
        {
            "VonHeikemen/lsp-zero.nvim",
            branch = "v1.x",
            dependencies = {
                -- LSP Support
                { "neovim/nvim-lspconfig" },
                { "williamboman/mason.nvim" },
                { "williamboman/mason-lspconfig.nvim" },

                -- Autocompletion
                { "hrsh7th/nvim-cmp" },
                { "hrsh7th/cmp-buffer" },
                { "hrsh7th/cmp-path" },
                { "saadparwaiz1/cmp_luasnip" },
                { "hrsh7th/cmp-nvim-lsp" },
                { "hrsh7th/cmp-nvim-lua" },

                -- Snippets
                {
                    "L3MON4D3/LuaSnip",
                    run = "make install_jsregexp"
                },
                { "rafamadriz/friendly-snippets" },
            },
            lazy = true,
        },
        -- lsp lines diagnostique
        { "https://git.sr.ht/~whynothugo/lsp_lines.nvim", lazy = true },
        -- file explorer
        {
            "nvim-tree/nvim-tree.lua",
            dependencies = {
                "nvim-tree/nvim-web-devicons", -- optional
            },
            tag = "v1.7.0",
            lazy = true
        },
        -- notification
        {
            "rcarriga/nvim-notify",
            lazy = true
        },

        -- UI cmd line, message, and popup
        {
            "folke/noice.nvim",
            dependencies = {
                "MunifTanjim/nui.nvim",
                "rcarriga/nvim-notify"
            },
            event = "VeryLazy",
        },
        -- rename all occurences
        {
            "smjonas/inc-rename.nvim",
            lazy = true
        },
        -- error diagnostic
        {
            "folke/trouble.nvim",
            dependencies = { "nvim-tree/nvim-web-devicons" },
            lazy = true,
        },
        -- data bases
        { "tpope/vim-dadbod" },
        { "kristijanhusak/vim-dadbod-ui" },
        -- git
        { "lewis6991/gitsigns.nvim",                      lazy = true },
        { "sindrets/diffview.nvim",                       lazy = true },
        {
            "akinsho/git-conflict.nvim",
            version = "*",
            lazy = true
        },
        -- buffer line
        {
            "nvim-lualine/lualine.nvim",
            dependencies = { "nvim-tree/nvim-web-devicons", opt = true },
            lazy = true
        },
        -- buffer bar
        {
            "akinsho/bufferline.nvim",
            version = "*",
            dependencies = { "nvim-tree/nvim-web-devicons" },
            lazy = true
        },
        -- window bar
        { "fgheng/winbar.nvim",                  lazy = true },
        -- highlight color by language
        { "nvim-treesitter/nvim-treesitter",     run = ":TSUpdate",         lazy = true, enabled = true },
        -- treesitter
        { "nvim-treesitter/playground",          lazy = true },
        -- indent blank line
        { "lukas-reineke/indent-blankline.nvim", tag = "v2.20.8",           lazy = true },
        -- auto close tag
        { "windwp/nvim-ts-autotag",              after = "nvim-treesitter", lazy = true },
        -- auto pair "'([{}])'"
        {
            "windwp/nvim-autopairs",
            config = function()
                require("nvim-autopairs").setup({})
            end,
        },
        -- nerd font icons
        { "nvim-tree/nvim-web-devicons" },
        -- comment lines
        { "numToStr/Comment.nvim" },
        -- colorizer
        { "NvChad/nvim-colorizer.lua",  lazy = true },
        -- test plugin
        {
            "nvim-neotest/neotest",
            dependencies = {
                "nvim-lua/plenary.nvim",
                "nvim-treesitter/nvim-treesitter",
                "antoinemadec/FixCursorHold.nvim",
                "olimorris/neotest-phpunit",
                "nvim-neotest/neotest-plenary",
            },
            lazy = true,
        },
        {
            "praem90/neotest-docker-phpunit.nvim",
            dependencies = {
                "nvim-neotest/neotest",
                "olimorris/neotest-phpunit",
            },
            lazy = true,
        },
        -- debug plugin
        { "williamboman/mason.nvim",      lazy = true },
        { "jay-babu/mason-nvim-dap.nvim", lazy = true },
        { "mfussenegger/nvim-dap",        lazy = true },
        {
            "rcarriga/nvim-dap-ui",
            dependencies = { "mfussenegger/nvim-dap" },
            lazy = true,
        },
        { "theHamsta/nvim-dap-virtual-text",   lazy = true },
        { "nvim-telescope/telescope-dap.nvim", lazy = true },
        -- neovim dashboard
        {
            "goolord/alpha-nvim",
            dependencies = { "nvim-tree/nvim-web-devicons" },
            lazy = true,
        },
    },
    -- automatically check for plugin updates
    checker = { enabled = true },
})
