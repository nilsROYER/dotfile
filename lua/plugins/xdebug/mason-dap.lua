require("mason").setup()
require("mason-nvim-dap").setup({
    ensure_installed = { 'php' },
    handlers = {}, -- sets up dap in the predefined manner
})
