local dap_ok, dap = pcall(require, 'dap')

if not dap_ok then
    return
end
dap.adapters.php = {
    type = 'executable',
    command = 'node',
    args = { '/vscode-php-debug/out/phpDebug.js' }
}

dap.configurations.php = {
    {
        type = 'php',
        request = 'launch',
        name = 'Listen for Xdebug',
        port = 9003,
    }
}
