local tree_conf_status, nvim_treesitter_configs = pcall(require, "nvim-treesitter.configs")
if not tree_conf_status then
    return
end

local nvim_treesitter_install_ok, nvim_treesitter_install = pcall(require, "nvim-treesitter.install")
if not nvim_treesitter_install_ok then
    return
end

nvim_treesitter_install.prefer_git = true

nvim_treesitter_configs.setup({
    -- Add languages to be installed here that you want installed for treesitter
    ensure_installed = {
        "bash",
        "c",
        "cpp",
        "c_sharp",
        "css",
        "scss",
        "graphql",
        "html",
        "javascript",
        "typescript",
        "json",
        "php",
        "rust",
        "scss",
        "lua",
        "vim",
        "sql",
        "dockerfile",
        "markdown",
        "markdown_inline",
        "gitignore",
        "regex",
    },
    autotag = { enable = true }, -- use nvim-ts-autotag plugin
    highlight = {
        enable = true,
        -- Setting this to true will run `:h syntax` and tree-sitter at the same time.
        -- Set this to `true` if you depend on 'syntax' being enabled (like for indentation).
        -- Using this option may slow down your editor, and you may see some duplicate highlights.
        -- Instead of true it can also be a list of languages
        additional_vim_regex_highlighting = false,
    },
    indent = { enable = true },
})
