-- local theme = "night"
-- local theme = "storm"
-- local theme = "day"
local theme = "moon"
vim.cmd("colorscheme tokyonight-" .. theme)

if theme == "night" or theme == "storm" then
    lualine_theme = "onedark"
elseif theme == "moon" then
    lualine_theme = "palenight"
else
    lualine_theme = "onelight"
end
