-- local theme = "lotus"
local theme = "wave"
-- local theme = "dragon"
vim.cmd("colorscheme kanagawa-" .. theme)
-- require("kanagawa").load("wave")
-- require("kanagawa").load("draggon")
-- require("kanagawa").load("lotus")
if theme == "lotus" then
    lualine_theme = "gruvbox_light"
else
    lualine_theme = "onedark"
end

require("plugins.lualine")
