vim.cmd("syntax enable")

local options = {
    wrap = false,
    signcolumn = "yes",
    ruler = true, -- show the cursor position all the time
    hidden = true,
    splitbelow = true,
    splitright = true,  -- Vertical splits will automatically be below
    smarttab = true,    -- Change the number of space characters inserted for indentation
    expandtab = true,   -- convert tabs to spaces
    smartindent = true, -- good auto indent
    number = true,      -- displat line numbers
    relativenumber = true,
    cursorline = true,  -- enable highlighting of the current  lineopt.encoding = 'utf-8'
    textwidth = 120,
    colorcolumn = "120",
    pumheight = 10,
    cmdheight = 2,
    conceallevel = 0,
    tabstop = 2,
    shiftwidth = 4,   -- change the number of space characters inserted for indentation
    showtabline = 2,  -- always show tabs
    updatetime = 300, -- faster completion
    timeoutlen = 500, -- by default timeoutlen is 1000 ms
    laststatus = 3,
    scrolloff = 10,
    sidescrolloff = 10,
    fileencoding = "utf-8",
    -- background = 'dark',       -- background color
    mouse = "a",
    clipboard = "unnamedplus", -- copy paste between vim and everything else

    swapfile = false,
    backup = false,
    undodir = os.getenv("HOME") .. "/.vim/undodir",
    undofile = true,

    hlsearch = false,
    incsearch = true,
    ignorecase = true,
    smartcase = true,
    showmode = false, -- We don't need to see things like -- INSERT -- anymore
    termguicolors = true,
}

local generals = {
    mapleader = " ",
    nobackup = true,      -- thiss recommended by coc
    nowritebackup = true, -- this is recommended by coc
    notgc = true,         -- this is recommended by coc
    autoindent = true,
    guibg = false,
}

for option, value in pairs(options) do
    vim.opt[option] = value
end

for general, value in pairs(generals) do
    vim.g[general] = value
end

vim.opt.formatoptions:remove("cro") -- stop newline continuation of comments
