local keymap = vim.keymap.set
local full_options = { noremap = true, silent = true }
local noremap = { noremap = true }

local buildin_ok, builtin = pcall(require, "telescope.builtin")
if not buildin_ok then
    return
end

-- save file
keymap("n", "<C-s>", ":w <CR>")
keymap("n", "<C-Q>", ":wq! | source % <CR>")

-- navigate buffer
keymap("n", "<TAB>", ":bnext<CR>", noremap)
keymap("n", "<S-TAB>", ":bprevious<CR>", noremap)

-- resize windows commands
keymap("n", "<M-k>", ":resize +2<CR>", full_options)
keymap("n", "<M-h>", ":vertical -2<CR>", full_options)
keymap("n", "<M-l>", ":vertical +2<CR>", full_options)
keymap("n", "<M-j>", ":resize +2<CR>", full_options)

keymap("v", "<C-j>", ":move '>+1<CR>gv=gv", full_options)
keymap("v", "<C-k>", ":move '<-2<CR>gv=gv", full_options)
keymap("n", "<C-j>", ":move .+1<CR>==", full_options)
keymap("n", "<C-k>", ":move .-2<CR>==", full_options)

-- treesiter plugin
keymap("n", "<leader>n", ":NvimTreeToggle<CR>", full_options)
keymap("n", "<leader>f", ":NvimTreeFindFile<CR>", full_options)

-- telescope plugin
keymap("n", "<C-n>", builtin.find_files, full_options)
keymap("n", "<C-f>", builtin.live_grep, full_options)
keymap("n", "<leader>fb", builtin.buffers, full_options)
keymap("n", "<leader>fh", builtin.help_tags, full_options)

-- xdebug
keymap("n", "<F1>", ":lua require'dap'.continue()<cr>", full_options)
keymap("n", "<F2>", ":lua require'dap'.step_over()<cr>", full_options)
keymap("n", "<F3>", ":lua require'dap'.step_into()<cr>", full_options)
keymap("n", "<leader>b", ":lua require'dap'.toggle_breakpoint()<cr>", full_options)

-- lsp_lines
keymap("n", "<leader>l", ":lua require'lsp_lines'.toggle()<cr>", full_options)

-- inc_rename (rename all word occurences)
keymap('n', '<leader><F6>', function()
    return ':IncRename ' .. vim.fn.expand("<cword>")
end, { expr = true })
